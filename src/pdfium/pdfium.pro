TARGET = QtPdfium

DEFINES += __QT__ \
    OPJ_STATIC \
    PNG_PREFIX \
    PNG_USE_READ_MACROS

# This is to prevent an undefined reference of qt_version_tag
# when on Linux, x86 architecture and the GNU tools.
DEFINES += QT_NO_VERSION_TAGGING WIN64

QT = core-private core gui
CONFIG += warn_on strict_flags c++11
load(qt_module)

#QMAKE_DOCS = $$PWD/doc/qtpdfium.qdocconf
include($$PWD/../3rdparty/pdfium.pri)

#LIBS += kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib
LIBS += $$PWD/../../winLibs/x64/kernel32.lib
LIBS += $$PWD/../../winLibs/x64/user32.lib
LIBS += $$PWD/../../winLibs/x64/gdi32.lib
LIBS += $$PWD/../../winLibs/x64/winspool.lib
LIBS += $$PWD/../../winLibs/x64/comdlg32.lib
LIBS += $$PWD/../../winLibs/x64/advapi32.lib
LIBS += $$PWD/../../winLibs/x64/shell32.lib
LIBS += $$PWD/../../winLibs/x64/ole32.lib

PRIVATE_HEADERS += \
    $$PWD/qpdfiumglobal.h

PUBLIC_HEADERS += \
    $$PWD/qpdfium.h \
    $$PWD/qpdfiumpage.h
SOURCES += \
    $$PWD/qpdfiumglobal.cpp \
    $$PWD/qpdfium.cpp \
    $$PWD/qpdfiumpage.cpp

HEADERS += $$PUBLIC_HEADERS $$PRIVATE_HEADERS

