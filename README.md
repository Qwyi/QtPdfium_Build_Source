# QtPdfium 库

QtPdfium is a Qt module that renders PDF files using the pdfium project by google. It render PDF pages into QImage.


### 工程说明
#### 文件目录说明
主要目录说明如下：
    - qtpdfiumBuild: 为基于Qt 5.12.11 mingw 64位 编译的 qt版本动态和静态库文件，以及头文件，可直接添加使用
    - sourceZib: 为原始的可编译的qt-pdfium 源代码压缩包文件
    - src: 为编译时需要的源码文件
    - winLibs: 为编译时需要使用的windows 库文件，包括x32和x64位库，目前编译的采用x64位库

### 使用说明

The API consists in only two classes: `QPdfium` and `QPdfiumPage`.

To load a PDF file, simply do:
```cpp
#include <QtPdfium/QPdfium>

QPdfium pdf("mypdf.pdf");
qDebug() << pdf.isValid(); //should return true
```

You can get the number of pages with `QPdfium::pageCount()`

You can access pages by index:

```cpp
#include "qpdfium.h"

QPdfium pdf("mypdf.pdf");
QPdfiumPage page =  pdf.page(0);
qDebug() << page.width(); // should show something!
```

To generate the image:

```cpp
#include "qpdfium.h"

QPdfium pdf("mypdf.pdf");
QPdfiumPage page =  pdf.page(0);
QImage image = page.image(3); //renders it 3 times bigger
```

And for extracting text:

```cpp
#include "qpdfium.h"

QPdfium pdf("mypdf.pdf");
QPdfiumPage page =  pdf.page(0);
QString pageText = page.text();
```



### License
BSD
