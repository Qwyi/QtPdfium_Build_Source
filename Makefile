#############################################################################
# Makefile for building: qtpdfium
# Generated by qmake (3.1) (Qt 5.9.1)
# Project:  qtpdfium.pro
# Template: subdirs
# Command: D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\bin\qmake.exe -o makefile qtpdfium.pro
#############################################################################

MAKEFILE      = makefile

first: make_first
QMAKE         = D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\bin\qmake.exe
DEL_FILE      = del
CHK_DIR_EXISTS= if not exist
MKDIR         = mkdir
COPY          = copy /y
COPY_FILE     = copy /y
COPY_DIR      = xcopy /s /q /y /i
INSTALL_FILE  = copy /y
INSTALL_PROGRAM = copy /y
INSTALL_DIR   = xcopy /s /q /y /i
QINSTALL      = D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\bin\qmake.exe -install qinstall
QINSTALL_PROGRAM = D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\bin\qmake.exe -install qinstall -exe
DEL_FILE      = del
SYMLINK       = $(QMAKE) -install ln -f -s
DEL_DIR       = rmdir
MOVE          = move
SUBTARGETS    =  \
		sub-src \
		sub-examples \
		sub-tests


sub-src-qmake_all:  FORCE
	@if not exist src\ mkdir src\ & if not exist src\ exit 1
	cd src\ && $(QMAKE) -o Makefile E:\Qt_PDF\qtpdfium\src\src.pro
	@set MAKEFLAGS=$(MAKEFLAGS)
	cd src\ && $(MAKE) -f Makefile qmake_all
sub-src: FORCE
	@if not exist src\ mkdir src\ & if not exist src\ exit 1
	@set MAKEFLAGS=$(MAKEFLAGS)
	cd src\ && ( if not exist Makefile $(QMAKE) -o Makefile E:\Qt_PDF\qtpdfium\src\src.pro ) && $(MAKE) -f Makefile
sub-src-make_first: FORCE
	@if not exist src\ mkdir src\ & if not exist src\ exit 1
	@set MAKEFLAGS=$(MAKEFLAGS)
	cd src\ && ( if not exist Makefile $(QMAKE) -o Makefile E:\Qt_PDF\qtpdfium\src\src.pro ) && $(MAKE) -f Makefile 
sub-src-all: FORCE
	@if not exist src\ mkdir src\ & if not exist src\ exit 1
	@set MAKEFLAGS=$(MAKEFLAGS)
	cd src\ && ( if not exist Makefile $(QMAKE) -o Makefile E:\Qt_PDF\qtpdfium\src\src.pro ) && $(MAKE) -f Makefile all
sub-src-clean: FORCE
	@if not exist src\ mkdir src\ & if not exist src\ exit 1
	@set MAKEFLAGS=$(MAKEFLAGS)
	cd src\ && ( if not exist Makefile $(QMAKE) -o Makefile E:\Qt_PDF\qtpdfium\src\src.pro ) && $(MAKE) -f Makefile clean
sub-src-distclean: FORCE
	@if not exist src\ mkdir src\ & if not exist src\ exit 1
	@set MAKEFLAGS=$(MAKEFLAGS)
	cd src\ && ( if not exist Makefile $(QMAKE) -o Makefile E:\Qt_PDF\qtpdfium\src\src.pro ) && $(MAKE) -f Makefile distclean
sub-src-install_subtargets: FORCE
	@if not exist src\ mkdir src\ & if not exist src\ exit 1
	@set MAKEFLAGS=$(MAKEFLAGS)
	cd src\ && ( if not exist Makefile $(QMAKE) -o Makefile E:\Qt_PDF\qtpdfium\src\src.pro ) && $(MAKE) -f Makefile install
sub-src-uninstall_subtargets: FORCE
	@if not exist src\ mkdir src\ & if not exist src\ exit 1
	@set MAKEFLAGS=$(MAKEFLAGS)
	cd src\ && ( if not exist Makefile $(QMAKE) -o Makefile E:\Qt_PDF\qtpdfium\src\src.pro ) && $(MAKE) -f Makefile uninstall
sub-examples-qmake_all: sub-src-qmake_all FORCE
	@if not exist examples\ mkdir examples\ & if not exist examples\ exit 1
	cd examples\ && $(QMAKE) -o Makefile E:\Qt_PDF\qtpdfium\examples\examples.pro
	@set MAKEFLAGS=$(MAKEFLAGS)
	cd examples\ && $(MAKE) -f Makefile qmake_all
sub-examples: sub-src FORCE
	@if not exist examples\ mkdir examples\ & if not exist examples\ exit 1
	@set MAKEFLAGS=$(MAKEFLAGS)
	cd examples\ && ( if not exist Makefile $(QMAKE) -o Makefile E:\Qt_PDF\qtpdfium\examples\examples.pro ) && $(MAKE) -f Makefile
sub-examples-make_first: sub-src-make_first FORCE
	@if not exist examples\ mkdir examples\ & if not exist examples\ exit 1
	@set MAKEFLAGS=$(MAKEFLAGS)
	cd examples\ && ( if not exist Makefile $(QMAKE) -o Makefile E:\Qt_PDF\qtpdfium\examples\examples.pro ) && $(MAKE) -f Makefile 
sub-examples-all: sub-src-all FORCE
	@if not exist examples\ mkdir examples\ & if not exist examples\ exit 1
	@set MAKEFLAGS=$(MAKEFLAGS)
	cd examples\ && ( if not exist Makefile $(QMAKE) -o Makefile E:\Qt_PDF\qtpdfium\examples\examples.pro ) && $(MAKE) -f Makefile all
sub-examples-clean: sub-src-clean FORCE
	@if not exist examples\ mkdir examples\ & if not exist examples\ exit 1
	@set MAKEFLAGS=$(MAKEFLAGS)
	cd examples\ && ( if not exist Makefile $(QMAKE) -o Makefile E:\Qt_PDF\qtpdfium\examples\examples.pro ) && $(MAKE) -f Makefile clean
sub-examples-distclean: sub-src-distclean FORCE
	@if not exist examples\ mkdir examples\ & if not exist examples\ exit 1
	@set MAKEFLAGS=$(MAKEFLAGS)
	cd examples\ && ( if not exist Makefile $(QMAKE) -o Makefile E:\Qt_PDF\qtpdfium\examples\examples.pro ) && $(MAKE) -f Makefile distclean
sub-examples-install_subtargets: sub-src-install_subtargets FORCE
	@if not exist examples\ mkdir examples\ & if not exist examples\ exit 1
	@set MAKEFLAGS=$(MAKEFLAGS)
	cd examples\ && ( if not exist Makefile $(QMAKE) -o Makefile E:\Qt_PDF\qtpdfium\examples\examples.pro ) && $(MAKE) -f Makefile install
sub-examples-uninstall_subtargets: sub-src-uninstall_subtargets FORCE
	@if not exist examples\ mkdir examples\ & if not exist examples\ exit 1
	@set MAKEFLAGS=$(MAKEFLAGS)
	cd examples\ && ( if not exist Makefile $(QMAKE) -o Makefile E:\Qt_PDF\qtpdfium\examples\examples.pro ) && $(MAKE) -f Makefile uninstall
sub-tests-qmake_all: sub-src-qmake_all FORCE
	@if not exist tests\ mkdir tests\ & if not exist tests\ exit 1
	cd tests\ && $(QMAKE) -o Makefile E:\Qt_PDF\qtpdfium\tests\tests.pro
	@set MAKEFLAGS=$(MAKEFLAGS)
	cd tests\ && $(MAKE) -f Makefile qmake_all
sub-tests: sub-src FORCE
	@if not exist tests\ mkdir tests\ & if not exist tests\ exit 1
	@set MAKEFLAGS=$(MAKEFLAGS)
	cd tests\ && ( if not exist Makefile $(QMAKE) -o Makefile E:\Qt_PDF\qtpdfium\tests\tests.pro ) && $(MAKE) -f Makefile
sub-tests-make_first: sub-src-make_first FORCE
	@if not exist tests\ mkdir tests\ & if not exist tests\ exit 1
	@set MAKEFLAGS=$(MAKEFLAGS)
	cd tests\ && ( if not exist Makefile $(QMAKE) -o Makefile E:\Qt_PDF\qtpdfium\tests\tests.pro ) && $(MAKE) -f Makefile 
sub-tests-all: sub-src-all FORCE
	@if not exist tests\ mkdir tests\ & if not exist tests\ exit 1
	@set MAKEFLAGS=$(MAKEFLAGS)
	cd tests\ && ( if not exist Makefile $(QMAKE) -o Makefile E:\Qt_PDF\qtpdfium\tests\tests.pro ) && $(MAKE) -f Makefile all
sub-tests-clean: sub-src-clean FORCE
	@if not exist tests\ mkdir tests\ & if not exist tests\ exit 1
	@set MAKEFLAGS=$(MAKEFLAGS)
	cd tests\ && ( if not exist Makefile $(QMAKE) -o Makefile E:\Qt_PDF\qtpdfium\tests\tests.pro ) && $(MAKE) -f Makefile clean
sub-tests-distclean: sub-src-distclean FORCE
	@if not exist tests\ mkdir tests\ & if not exist tests\ exit 1
	@set MAKEFLAGS=$(MAKEFLAGS)
	cd tests\ && ( if not exist Makefile $(QMAKE) -o Makefile E:\Qt_PDF\qtpdfium\tests\tests.pro ) && $(MAKE) -f Makefile distclean
sub-tests-install_subtargets: sub-src-install_subtargets FORCE
	@if not exist tests\ mkdir tests\ & if not exist tests\ exit 1
	@set MAKEFLAGS=$(MAKEFLAGS)
	cd tests\ && ( if not exist Makefile $(QMAKE) -o Makefile E:\Qt_PDF\qtpdfium\tests\tests.pro ) && $(MAKE) -f Makefile install
sub-tests-uninstall_subtargets: sub-src-uninstall_subtargets FORCE
	@if not exist tests\ mkdir tests\ & if not exist tests\ exit 1
	@set MAKEFLAGS=$(MAKEFLAGS)
	cd tests\ && ( if not exist Makefile $(QMAKE) -o Makefile E:\Qt_PDF\qtpdfium\tests\tests.pro ) && $(MAKE) -f Makefile uninstall

makefile: qtpdfium.pro .qmake.conf .qmake.cache D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\win32-msvc\qmake.conf D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\features\spec_pre.prf \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\common\angle.conf \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\common\msvc-desktop.conf \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\qconfig.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_3danimation.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_3danimation_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_3dcore.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_3dcore_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_3dextras.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_3dextras_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_3dinput.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_3dinput_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_3dlogic.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_3dlogic_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_3dquick.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_3dquick_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_3dquickanimation.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_3dquickanimation_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_3dquickextras.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_3dquickextras_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_3dquickinput.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_3dquickinput_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_3dquickrender.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_3dquickrender_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_3dquickscene2d.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_3dquickscene2d_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_3drender.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_3drender_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_accessibility_support_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_axbase.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_axbase_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_axcontainer.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_axcontainer_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_axserver.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_axserver_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_bluetooth.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_bluetooth_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_bootstrap_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_charts.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_charts_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_concurrent.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_concurrent_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_core.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_core_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_datavisualization.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_datavisualization_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_dbus.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_dbus_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_designer.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_designer_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_designercomponents_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_devicediscovery_support_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_egl_support_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_eventdispatcher_support_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_fb_support_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_fontdatabase_support_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_gamepad.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_gamepad_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_gui.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_gui_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_help.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_help_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_location.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_location_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_multimedia.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_multimedia_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_multimediawidgets.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_multimediawidgets_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_network.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_network_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_networkauth.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_networkauth_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_nfc.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_nfc_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_opengl.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_opengl_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_openglextensions.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_openglextensions_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_packetprotocol_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_pdfium.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_pdfium_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_platformcompositor_support_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_positioning.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_positioning_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_printsupport.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_printsupport_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_purchasing.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_purchasing_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_qml.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_qml_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_qmldebug_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_qmldevtools_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_qmltest.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_qmltest_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_qtmultimediaquicktools_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_quick.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_quick_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_quickcontrols2.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_quickcontrols2_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_quickparticles_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_quicktemplates2_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_quickwidgets.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_quickwidgets_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_remoteobjects.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_remoteobjects_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_repparser.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_repparser_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_script.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_script_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_scripttools.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_scripttools_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_scxml.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_scxml_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_sensors.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_sensors_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_serialbus.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_serialbus_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_serialport.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_serialport_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_sql.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_sql_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_svg.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_svg_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_testlib.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_testlib_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_texttospeech.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_texttospeech_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_theme_support_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_uiplugin.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_uitools.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_uitools_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_webchannel.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_webchannel_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_webengine.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_webengine_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_webenginecore.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_webenginecore_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_webenginecoreheaders_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_webenginewidgets.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_webenginewidgets_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_websockets.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_websockets_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_webview.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_webview_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_widgets.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_widgets_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_winextras.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_winextras_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_xml.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_xml_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_xmlpatterns.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_xmlpatterns_private.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_zlib_private.pri \
		mkspecs\modules-inst\qt_lib_pdfium.pri \
		mkspecs\modules-inst\qt_lib_pdfium_private.pri \
		mkspecs\modules\qt_lib_pdfium.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\features\qt_functions.prf \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\features\qt_config.prf \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\win32-msvc\qmake.conf \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\features\spec_post.prf \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\qmodule.pri \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\features\qt_build_config.prf \
		.qmake.conf \
		.qmake.cache \
		.qmake.stash \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\features\exclusive_builds.prf \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\common\msvc-version.conf \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\features\toolchain.prf \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\features\default_pre.prf \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\features\win32\default_pre.prf \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\features\qt_configure.prf \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\features\qt_parts.prf \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\features\resolve_config.prf \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\features\exclusive_builds_post.prf \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\features\default_post.prf \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\features\qt_example_installs.prf \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\features\exceptions_off.prf \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\features\qt_docs_targets.prf \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\features\precompile_header.prf \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\features\warn_on.prf \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\features\qmake_use.prf \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\features\file_copies.prf \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\features\qt_build_extra.prf \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\features\win32\windows.prf \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\features\testcase_targets.prf \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\features\yacc.prf \
		D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\features\lex.prf \
		qtpdfium.pro
	$(QMAKE) -o makefile qtpdfium.pro
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\features\spec_pre.prf:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\common\angle.conf:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\common\msvc-desktop.conf:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\qconfig.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_3danimation.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_3danimation_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_3dcore.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_3dcore_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_3dextras.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_3dextras_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_3dinput.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_3dinput_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_3dlogic.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_3dlogic_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_3dquick.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_3dquick_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_3dquickanimation.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_3dquickanimation_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_3dquickextras.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_3dquickextras_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_3dquickinput.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_3dquickinput_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_3dquickrender.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_3dquickrender_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_3dquickscene2d.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_3dquickscene2d_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_3drender.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_3drender_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_accessibility_support_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_axbase.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_axbase_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_axcontainer.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_axcontainer_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_axserver.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_axserver_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_bluetooth.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_bluetooth_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_bootstrap_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_charts.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_charts_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_concurrent.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_concurrent_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_core.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_core_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_datavisualization.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_datavisualization_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_dbus.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_dbus_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_designer.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_designer_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_designercomponents_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_devicediscovery_support_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_egl_support_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_eventdispatcher_support_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_fb_support_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_fontdatabase_support_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_gamepad.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_gamepad_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_gui.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_gui_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_help.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_help_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_location.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_location_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_multimedia.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_multimedia_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_multimediawidgets.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_multimediawidgets_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_network.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_network_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_networkauth.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_networkauth_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_nfc.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_nfc_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_opengl.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_opengl_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_openglextensions.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_openglextensions_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_packetprotocol_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_pdfium.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_pdfium_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_platformcompositor_support_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_positioning.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_positioning_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_printsupport.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_printsupport_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_purchasing.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_purchasing_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_qml.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_qml_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_qmldebug_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_qmldevtools_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_qmltest.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_qmltest_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_qtmultimediaquicktools_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_quick.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_quick_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_quickcontrols2.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_quickcontrols2_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_quickparticles_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_quicktemplates2_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_quickwidgets.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_quickwidgets_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_remoteobjects.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_remoteobjects_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_repparser.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_repparser_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_script.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_script_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_scripttools.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_scripttools_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_scxml.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_scxml_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_sensors.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_sensors_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_serialbus.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_serialbus_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_serialport.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_serialport_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_sql.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_sql_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_svg.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_svg_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_testlib.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_testlib_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_texttospeech.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_texttospeech_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_theme_support_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_uiplugin.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_uitools.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_uitools_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_webchannel.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_webchannel_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_webengine.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_webengine_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_webenginecore.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_webenginecore_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_webenginecoreheaders_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_webenginewidgets.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_webenginewidgets_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_websockets.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_websockets_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_webview.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_webview_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_widgets.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_widgets_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_winextras.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_winextras_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_xml.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_xml_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_xmlpatterns.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_xmlpatterns_private.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\modules\qt_lib_zlib_private.pri:
mkspecs\modules-inst\qt_lib_pdfium.pri:
mkspecs\modules-inst\qt_lib_pdfium_private.pri:
mkspecs\modules\qt_lib_pdfium.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\features\qt_functions.prf:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\features\qt_config.prf:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\win32-msvc\qmake.conf:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\features\spec_post.prf:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\qmodule.pri:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\features\qt_build_config.prf:
.qmake.conf:
.qmake.cache:
.qmake.stash:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\features\exclusive_builds.prf:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\common\msvc-version.conf:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\features\toolchain.prf:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\features\default_pre.prf:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\features\win32\default_pre.prf:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\features\qt_configure.prf:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\features\qt_parts.prf:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\features\resolve_config.prf:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\features\exclusive_builds_post.prf:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\features\default_post.prf:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\features\qt_example_installs.prf:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\features\exceptions_off.prf:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\features\qt_docs_targets.prf:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\features\precompile_header.prf:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\features\warn_on.prf:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\features\qmake_use.prf:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\features\file_copies.prf:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\features\qt_build_extra.prf:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\features\win32\windows.prf:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\features\testcase_targets.prf:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\features\yacc.prf:
D:\Qt\Qt5.9.1\5.9.1\msvc2015_64\mkspecs\features\lex.prf:
qtpdfium.pro:
qmake: FORCE
	@$(QMAKE) -o makefile qtpdfium.pro

qmake_all: sub-src-qmake_all sub-examples-qmake_all sub-tests-qmake_all FORCE

make_first: sub-src-make_first  FORCE
all: sub-src-all sub-examples-all sub-tests-all  FORCE
clean: sub-src-clean sub-examples-clean sub-tests-clean  FORCE
distclean: sub-src-distclean sub-examples-distclean sub-tests-distclean  FORCE
	-$(DEL_FILE) makefile
	-$(DEL_FILE) .qmake.cache config.cache config.log mkspecs\modules\*.pri mkspecs\modules-inst\*.pri .qmake.stash
install_subtargets: sub-src-install_subtargets FORCE
uninstall_subtargets: sub-src-uninstall_subtargets FORCE

sub-src-debug:
	@if not exist src\ mkdir src\ & if not exist src\ exit 1
	@set MAKEFLAGS=$(MAKEFLAGS)
	cd src\ && ( if not exist Makefile $(QMAKE) -o Makefile E:\Qt_PDF\qtpdfium\src\src.pro ) && $(MAKE) -f Makefile debug
debug: sub-src-debug

sub-src-release:
	@if not exist src\ mkdir src\ & if not exist src\ exit 1
	@set MAKEFLAGS=$(MAKEFLAGS)
	cd src\ && ( if not exist Makefile $(QMAKE) -o Makefile E:\Qt_PDF\qtpdfium\src\src.pro ) && $(MAKE) -f Makefile release
release: sub-src-release

html_docs:
	$(MAKE) -f $(MAKEFILE) prepare_docs && $(MAKE) -f $(MAKEFILE) generate_docs

docs:
	$(MAKE) -f $(MAKEFILE) html_docs && $(MAKE) -f $(MAKEFILE) qch_docs

sub-src-install_html_docs:
	@if not exist src\ mkdir src\ & if not exist src\ exit 1
	@set MAKEFLAGS=$(MAKEFLAGS)
	cd src\ && ( if not exist Makefile $(QMAKE) -o Makefile E:\Qt_PDF\qtpdfium\src\src.pro ) && $(MAKE) -f Makefile install_html_docs
install_html_docs: sub-src-install_html_docs

sub-src-uninstall_html_docs:
	@if not exist src\ mkdir src\ & if not exist src\ exit 1
	@set MAKEFLAGS=$(MAKEFLAGS)
	cd src\ && ( if not exist Makefile $(QMAKE) -o Makefile E:\Qt_PDF\qtpdfium\src\src.pro ) && $(MAKE) -f Makefile uninstall_html_docs
uninstall_html_docs: sub-src-uninstall_html_docs

sub-src-install_qch_docs:
	@if not exist src\ mkdir src\ & if not exist src\ exit 1
	@set MAKEFLAGS=$(MAKEFLAGS)
	cd src\ && ( if not exist Makefile $(QMAKE) -o Makefile E:\Qt_PDF\qtpdfium\src\src.pro ) && $(MAKE) -f Makefile install_qch_docs
install_qch_docs: sub-src-install_qch_docs

sub-src-uninstall_qch_docs:
	@if not exist src\ mkdir src\ & if not exist src\ exit 1
	@set MAKEFLAGS=$(MAKEFLAGS)
	cd src\ && ( if not exist Makefile $(QMAKE) -o Makefile E:\Qt_PDF\qtpdfium\src\src.pro ) && $(MAKE) -f Makefile uninstall_qch_docs
uninstall_qch_docs: sub-src-uninstall_qch_docs

sub-src-install_docs:
	@if not exist src\ mkdir src\ & if not exist src\ exit 1
	@set MAKEFLAGS=$(MAKEFLAGS)
	cd src\ && ( if not exist Makefile $(QMAKE) -o Makefile E:\Qt_PDF\qtpdfium\src\src.pro ) && $(MAKE) -f Makefile install_docs
install_docs: sub-src-install_docs

sub-src-uninstall_docs:
	@if not exist src\ mkdir src\ & if not exist src\ exit 1
	@set MAKEFLAGS=$(MAKEFLAGS)
	cd src\ && ( if not exist Makefile $(QMAKE) -o Makefile E:\Qt_PDF\qtpdfium\src\src.pro ) && $(MAKE) -f Makefile uninstall_docs
uninstall_docs: sub-src-uninstall_docs

sub-src-qch_docs:
	@if not exist src\ mkdir src\ & if not exist src\ exit 1
	@set MAKEFLAGS=$(MAKEFLAGS)
	cd src\ && ( if not exist Makefile $(QMAKE) -o Makefile E:\Qt_PDF\qtpdfium\src\src.pro ) && $(MAKE) -f Makefile qch_docs
qch_docs: sub-src-qch_docs

sub-src-prepare_docs:
	@if not exist src\ mkdir src\ & if not exist src\ exit 1
	@set MAKEFLAGS=$(MAKEFLAGS)
	cd src\ && ( if not exist Makefile $(QMAKE) -o Makefile E:\Qt_PDF\qtpdfium\src\src.pro ) && $(MAKE) -f Makefile prepare_docs
prepare_docs: sub-src-prepare_docs

sub-src-generate_docs:
	@if not exist src\ mkdir src\ & if not exist src\ exit 1
	@set MAKEFLAGS=$(MAKEFLAGS)
	cd src\ && ( if not exist Makefile $(QMAKE) -o Makefile E:\Qt_PDF\qtpdfium\src\src.pro ) && $(MAKE) -f Makefile generate_docs
generate_docs: sub-src-generate_docs

sub-src-check:
	@if not exist src\ mkdir src\ & if not exist src\ exit 1
	@set MAKEFLAGS=$(MAKEFLAGS)
	cd src\ && ( if not exist Makefile $(QMAKE) -o Makefile E:\Qt_PDF\qtpdfium\src\src.pro ) && $(MAKE) -f Makefile check
check: sub-src-check

sub-src-benchmark:
	@if not exist src\ mkdir src\ & if not exist src\ exit 1
	@set MAKEFLAGS=$(MAKEFLAGS)
	cd src\ && ( if not exist Makefile $(QMAKE) -o Makefile E:\Qt_PDF\qtpdfium\src\src.pro ) && $(MAKE) -f Makefile benchmark
benchmark: sub-src-benchmark
install:install_subtargets  FORCE

uninstall: uninstall_subtargets FORCE

FORCE:

