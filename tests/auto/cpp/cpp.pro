TARGET = tst_cpp

QT += core concurrent pdfium testlib network
CONFIG += testcase c++11

INCLUDEPATH += ../../src/pdfium
VPATH += ../../../src/pdfium

SOURCES += \
    tst_cpp.cpp

DEFINES += "DATA=\\\"$$PWD/data\\\""

RESOURCES += \
    res.qrc


INCLUDEPATH += $$PWD/pdfium/include

LIBS += -L$$PWD/pdfium/lib

LIBS += -lQt5Pdfium

win32{
    # copy ffmpeg
    src_file = $$PWD/pdfium/bin/*
    dst_file = $$OUT_PWD
#    target_file = $$DESTDIR

    src_file ~= s,/,\\,g
    dst_file ~= s,/,\\,g
#    target_file ~= s,/,\\,g

    system(xcopy $$src_file $$dst_file /y /s/q/e)
#    system(xcopy $$src_file $$target_file /y /s/q/e)
}
