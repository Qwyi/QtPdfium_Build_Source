@echo off
SetLocal EnableDelayedExpansion
(set PATH=E:\Qt_PDF\qtpdfium\bin;D:\Qt\Qt5.9.1\5.9.1\msvc2015\bin;!PATH!)
if defined QT_PLUGIN_PATH (
    set QT_PLUGIN_PATH=D:\Qt\Qt5.9.1\5.9.1\msvc2015\plugins;!QT_PLUGIN_PATH!
) else (
    set QT_PLUGIN_PATH=D:\Qt\Qt5.9.1\5.9.1\msvc2015\plugins
)
%*
EndLocal
